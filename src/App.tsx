import React, { useState } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from "react-router-dom";
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Login from "./components/Login";
import SignUp from "./components/SignUp";
import useToken from "./hooks/useToken";
import Home from "./components/Home";

const App = () => {
  const {token} = useToken();

  return (<Router>
      <div className="App">
        <nav className="navbar navbar-expand-lg navbar-light fixed-top">
          <div className="container">
            <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
              <ul className="navbar-nav ml-auto">
                <li className="nav-item">
                  <Link className="nav-link" to={"/sign-in"}>Login</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to={"/sign-up"}>Sign up</Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <div className="auth-wrapper">
          <div className="auth-inner">
            <Switch>
              <Route
                exact
                path="/"
                render={() => {
                  return (
                    token ?
                      <Redirect to="/home"/> :
                      <Redirect to="/sign-in"/>
                  )
                }}
              />
              <Route path="/home" component={Home}/>
              <Route path="/sign-in" component={Login}/>
              <Route path="/sign-up" component={SignUp}/>
            </Switch>
          </div>
        </div>
      </div>
    </Router>
  );
}

export default App;
