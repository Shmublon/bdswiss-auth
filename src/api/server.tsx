interface Credentials {
  email: string;
  passwordHash: string;
}

interface RegData {
  firstName: string;
  lastName: string;
  email: string;
  passwordHash: string;
}

export const loginUser = async (credentials: Credentials) => {
  const response: any = await fetch('http://localhost:5000/api/v1/users/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(credentials)
  });
  if (response.status === 200) {
    const json = await response.json();
    return json.token;
  } else {
    return null;
  }
}

export const registerUser = async (regData: RegData) => {
  //TODO: encrypt password
  const response: any = await fetch('http://localhost:5000/api/v1/users/register', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(regData)
  });
  if (response.status === 200) {
    const json = await response.json();
    return json.token;
  } else {
    return null;
  }
}
