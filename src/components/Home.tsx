import React from "react";
import useToken from "../hooks/useToken";
import { Redirect } from "react-router-dom";

const Home: React.FC = () => {
  const {token} = useToken();

  if (!token) {
    return <Redirect to={'/sign-in'} />
  }
  return (
      <form>
        <h3>Sign Out</h3>
        <button onClick={() => localStorage.removeItem('token')} type="submit" className="btn btn-primary btn-block">Sign out</button>
      </form>
  );
}

export default Home;
