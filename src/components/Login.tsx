import React, { useState } from "react";
import { loginUser } from "../api/server";
import { Redirect } from "react-router-dom";
import useToken from "../hooks/useToken";

const Login: React.FC = () => {
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [error, setError] = useState<string>('');
  const {token, setToken} = useToken();

  if (token) {
    return <Redirect to={'/home'} />
  }

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    const token = await loginUser({
      email,
      passwordHash: password,
    });
    if (token) {
      setToken(token);
    } else {
      setError('Incorrect email or password')
    }
  }

  return (
      <form onSubmit={handleSubmit}>
        <h3>Sign In</h3>

        <div className="form-group">
          <label>Email address</label>
          <input onChange={(e: React.ChangeEvent<HTMLInputElement>) => setEmail(e.target.value)} type="email" className="form-control" placeholder="Enter email"/>
        </div>

        <div className="form-group">
          <label>Password</label>
          <input onChange={(e: React.ChangeEvent<HTMLInputElement>) => setPassword(e.target.value)} type="password" className="form-control" placeholder="Enter password"/>
        </div>

        <div className="form-group">
          <div className="custom-control custom-checkbox">
            <input type="checkbox" className="custom-control-input" id="customCheck1"/>
            <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
          </div>
        </div>

        <button type="submit" className="btn btn-primary btn-block">Submit</button>
        {error && <h3 className={'error'}>{error}</h3>}
        <p className="forgot-password text-right">
          Forgot <a href="/sign-up">password?</a>
        </p>
      </form>
  );
}

export default Login;
