import React, { useState } from "react";
import { registerUser } from "../api/server";
import { Redirect } from "react-router-dom";
import useToken from "../hooks/useToken";

const SignUp: React.FC = () => {
  const [firstName, setFirstName] = useState<string>('');
  const [lastName, setLastName] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const {token, setToken} = useToken();

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    const token = await registerUser({
      firstName,
      lastName,
      email,
      passwordHash: password,
    });
    setToken(token);
  }

  if (token) {
    return <Redirect to={'/home'} />
  }

  return (
    <form onSubmit={handleSubmit}>
      <h3>Sign Up</h3>

      <div className="form-group">
        <label>First name</label>
        <input onChange={(e: React.ChangeEvent<HTMLInputElement>) => setFirstName(e.target.value)} type="text" className="form-control" placeholder="First name" />
      </div>

      <div className="form-group">
        <label>Last name</label>
        <input onChange={(e: React.ChangeEvent<HTMLInputElement>) => setLastName(e.target.value)} type="text" className="form-control" placeholder="Last name" />
      </div>

      <div className="form-group">
        <label>Email address</label>
        <input onChange={(e: React.ChangeEvent<HTMLInputElement>) => setEmail(e.target.value)} type="email" className="form-control" placeholder="Enter email" />
      </div>

      <div className="form-group">
        <label>Password</label>
        <input onChange={(e: React.ChangeEvent<HTMLInputElement>) => setPassword(e.target.value)} type="password" className="form-control" placeholder="Enter password" />
      </div>

      <button type="submit" className="btn btn-primary btn-block">Sign Up</button>
      <p className="forgot-password text-right">
        Already registered <a href="/sign-in">sign in?</a>
      </p>
    </form>
  );
}

export default SignUp;
